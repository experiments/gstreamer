#!/bin/sh

gst-launch-1.0 -v interleave name=i ! \
  capssetter caps="audio/x-raw,channels=2,channel-mask=(bitmask)0x3" ! \
  audioconvert ! wavenc ! filesink location=audio-interleave-test.wav \
  audiotestsrc wave=0 num-buffers=100 ! audioconvert ! "audio/x-raw,channels=1" ! queue ! i.sink_0 \
  audiotestsrc wave=2 num-buffers=100 ! audioconvert ! "audio/x-raw,channels=1" ! queue ! i.sink_1
