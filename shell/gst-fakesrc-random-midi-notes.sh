#!/bin/sh

gst-launch-1.0 \
  fakesrc datarate=400 sizetype=2 sizemin=3 sizemax=3 num-buffers=1000 format=3 filltype=3 ! \
  audio/x-midi-event ! fluiddec ! audioconvert ! autoaudiosink
