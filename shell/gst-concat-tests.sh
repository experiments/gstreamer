#!/bin/sh

set -e
set -x

FILE1=sample_440hz.webm
FILE2=sample_880hz.webm

# This pipeline works fine
#gst-launch-1.0 \
#  concat name=c ! videoconvert ! videoscale ! autovideosink \
#  videotestsrc num-buffers=100 ! c. \
#  videotestsrc num-buffers=100 pattern=ball ! c.
 
# Here only the video from the first stream is shown
gst-launch-1.0 \
  concat name=c ! decodebin ! autovideosink \
  filesrc location="$FILE1" ! c. \
  filesrc location="$FILE2" ! c.

# This pipeline using one decoder per filesrc works fine
gst-launch-1.0 \
  concat name=c ! videoconvert ! videoscale ! autovideosink \
  filesrc location="$FILE1" ! matroskademux ! vp9dec ! c. \
  filesrc location="$FILE2" ! matroskademux ! vp9dec ! c.


# These pipelines work fine, but should a streamsynchronizer be added?
# And if so, where?
gst-launch-1.0 \
  concat name=video_concat ! autovideosink \
  concat name=audio_concat ! autoaudiosink \
  filesrc location="$FILE1" ! decodebin name=d1 \
  filesrc location="$FILE2" ! decodebin name=d2 \
  d1. ! video/x-raw ! queue ! video_concat. \
  d1. ! audio/x-raw ! queue ! audio_concat. \
  d2. ! video/x-raw ! queue ! video_concat. \
  d2. ! audio/x-raw ! queue ! audio_concat.

gst-launch-1.0 -e \
  mp4mux name=mp4 ! filesink location=out.mp4 \
  concat name=video_concat ! videoconvert ! x264enc tune=4 ! mp4. \
  concat name=audio_concat ! audioconvert ! voaacenc ! mp4. \
  filesrc location="$FILE1" ! decodebin name=d1 \
  filesrc location="$FILE2" ! decodebin name=d2 \
  d1. ! video/x-raw ! queue ! video_concat. \
  d1. ! audio/x-raw ! queue ! audio_concat. \
  d2. ! video/x-raw ! queue ! video_concat. \
  d2. ! audio/x-raw ! queue ! audio_concat.
