#!/bin/sh
#
# Test to check that the videoconvert background color is actually black.
#
# Depending on the pixelformat black can be represented in different ways, and
# videoconvert used to get some cases wrong.

set -e

PIPELINE='videotestsrc ! video/x-raw,format="$format",width=640,height=480 ! rotate angle="0.79" ! autovideoconvert ! autovideosink'
PIPELINE='videotestsrc ! video/x-raw,format="$format",width=640,height=480 ! videoscale add-borders=1 ! video/x-raw,width=800,height=480,pixel-aspect-ratio=1/1 ! videoconvert ! autovideosink'

FORMATS="ARGB BGR BGRA BGRx RGB RGBA RGBx AYUV xBGR xRGB GRAY8 GRAY16_BE GRAY16_LE"
for format in $FORMATS;
do
  echo "Testing format: $format"
  GST_PLUGIN_PATH=$(pwd) \
  eval gst-launch-1.0 $PIPELINE > /dev/null 2>&1
done
