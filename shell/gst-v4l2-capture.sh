#!/bin/sh

set -e

[ "x$1" = "x" ] && { echo "usage $(basename "$0") <destination_file>" 1>&2; exit 1; }

FILENAME="$1"

VIDEO_CODEC="video/x-raw,format=I420 ! jpegenc quality=90"

gst-launch-1.0 -e \
  matroskamux name=mux ! filesink location="$FILENAME" \
  v4l2src ! queue ! videoconvert ! videorate ! $VIDEO_CODEC ! queue ! mux. \
  pulsesrc ! queue ! audioconvert ! vorbisenc ! queue ! mux.
