#!/bin/sh

set -e

rm -rf dot_files
mkdir dot_files
GST_DEBUG_DUMP_DOT_DIR="$PWD/dot_files" \
  gst-launch-1.0 videotestsrc ! autovideosink
