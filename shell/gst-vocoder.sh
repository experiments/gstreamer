#!/bin/sh

FORMANT="autoaudiosrc"

# Connect your hardware MIDI device, or launch a virtual MIDI device like
# this:
#
#   $ xset -r && vkeybd; xset r on
#
# Find out the port of the input MIDI device:
#
#   $ aconnect -l
#   ...
#   client 128: 'Virtual Keyboard' [type=user,pid=25939]
#       0 'Virtual Keyboard'
#
# and have a lot of fun vocoding the crap out of your formant channel.
CARRIER_TONE="alsamidisrc ports=128:0 ! fluiddec synth-gain=1 ! audioconvert"

# Add some white noise to the carrier
CARRIER_PARAMS="$CARRIER_TONE ! carrier. audiotestsrc wave=5 volume=0.1 ! carrier."
CARRIER="adder name=carrier"

SINK="autoaudiosink"
#SINK="wavenc ! filesink location=audio-vocoder-test.wav"

# Using vocoder_1337.so, in Debian it's available in the swh-plugins package.
gst-launch-1.0 -v interleave name=i ! \
  capssetter caps="audio/x-raw,channels=2,channel-mask=(bitmask)0x3" ! audioconvert ! audioresample ! \
  ladspa-vocoder-1337-so-vocoder \
    number-of-bands=16 \
    left-right=0 \
    band-1-level=1 \
    band-2-level=1 \
    band-3-level=1 \
    band-4-level=1 \
    band-5-level=1 \
    band-6-level=1 \
    band-7-level=1 \
    band-8-level=1 \
    band-9-level=1 \
    band-10-level=1 \
    band-11-level=1 \
    band-12-level=1 \
    band-13-level=1 \
    band-14-level=1 \
    band-15-level=1 \
    band-16-level=1 \
  ! $SINK \
  $FORMANT ! audioconvert ! audioresample ! audio/x-raw, rate=44100, format=S16LE, channels=1 ! queue ! i.sink_0 \
  $CARRIER ! audioconvert ! audioresample ! audio/x-raw, rate=44100, format=S16LE, channels=1 ! queue ! i.sink_1 \
  $CARRIER_PARAMS
