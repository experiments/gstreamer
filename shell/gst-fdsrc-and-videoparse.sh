#!/bin/sh

set -e

# This is a very flexible way to read raw video data from the standard input
# when the image size, pixelformat and the framerate are known.

cat /dev/urandom |
gst-launch-1.0 \
  fdsrc blocksize=$((800*480*4)) ! \
  videoparse format="bgra" width=800 height=480 framerate=5 ! \
  queue min-threshold-buffers=5 ! \
  videoconvert ! videorate ! autovideosink
