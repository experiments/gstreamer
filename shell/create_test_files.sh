#!/bin/sh

set -e
set -x

SECONDS=4

VIDEO_FRAMERATE=25
VIDEO_BUFFERS=$(($VIDEO_FRAMERATE * $SECONDS))

AUDIO_SAMPLERATE=48000
AUDIO_SAMPLESPERBUFFER=1024
AUDIO_BUFFERS=$(($AUDIO_SAMPLERATE * $SECONDS / $AUDIO_SAMPLESPERBUFFER))

create_sample() {
  FREQ="$1"
  rm -f "sample_${FREQ}hz.webm"
  gst-launch-1.0 -e \
    webmmux name=mux ! filesink location="sample_${FREQ}hz.webm" \
    videotestsrc pattern=black num-buffers="$VIDEO_BUFFERS" ! video/x-raw,framerate="$VIDEO_FRAMERATE/1" ! \
        textoverlay valignment=center halignment=center font-desc="Mono, 72" text="${FREQ}Hz" ! \
        queue ! vp9enc ! mux. \
    audiotestsrc samplesperbuffer="$AUDIO_SAMPLESPERBUFFER" num-buffers="$AUDIO_BUFFERS" freq="$FREQ" ! audio/x-raw,rate="$AUDIO_SAMPLERATE" ! \
        queue ! audioconvert ! vorbisenc quality=0.5 ! queue ! mux.
}

create_sample 440
create_sample 880
