#!/bin/bash

set -e

usage() {
  echo "usage: $(basename $0) <input> <inpoint in HH:MM:SS> <duration in HH:MM:SS> <output>"
}

[ -f "$1" ] || { usage 1>&2; exit 1; }
INPUT_FILE="$1"

read INPOINT_HH INPOINT_MM INPOINT_SS <<< ${2//:/ }
read DURATION_HH DURATION_MM DURATION_SS <<< ${3//:/ }

# Remove leading zeros before performing the multiplications
INPOINT=$((${INPOINT_HH#0} * 60 * 60 + ${INPOINT_MM#0} * 60 + ${INPOINT_SS#0}))
DURATION=$((${DURATION_HH#0} * 60 * 60 + ${DURATION_MM#0} * 60 + ${DURATION_SS#0}))

[ -e "$4" ] && { echo "Output file already exists, bailing out!" 1>&2; exit 1; }
OUTPUT_FILE="$4"

ges-launch-1.0 +clip "$INPUT_FILE" inpoint=$INPOINT duration=$DURATION -o "$OUTPUT_FILE"
