#!/bin/sh

# example pipeline to download HSL streams from raiplay.it

set -e

{ [ "x$1" = "x" ] || [ "x$2" = x ]; } && { echo "usage: $(basename "$0") <source_m3u8_url> <output_file>" 1>&2; exit 1; }

SOURCE_URL="$1"
OUTPUT_FILE="$2"

# use the avimux element because my TV set does not like the output of mp4mux
# use a high value for connection-speed to download the best quality video
gst-launch-1.0 \
  \
  mp4mux name=mux ! \
  filesink location="$OUTPUT_FILE" \
  \
  "$SOURCE_URL" ! \
  hlsdemux connection-speed=4294967 ! \
  tsdemux name=demux \
  \
  demux.video_0_0100 ! queue ! h264parse ! queue ! \
  mux.video_00 \
  \
  demux.audio_0_0101 ! queue ! decodebin ! audioconvert ! lamemp3enc ! queue ! \
  mux.audio_00

# NOTE: the audio re-encoding is necessary because aacparse lacks some
# functionality, see:
# https://gitlab.freedesktop.org/gstreamer/gst-plugins-good/issues/106
