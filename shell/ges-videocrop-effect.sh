#!/bin/sh
#
# Example of how to apply a videocrop effect in a GES pipeline.
#
# Crop and Scale the circular pattern, and put into the white rectangle of the
# smtpe pattern.

set -e

CROP_TOP=40
CROP_BOTTOM=80
CROP_LEFT=100
CROP_RIGHT=110

POSX=52
POSY=180

WIDTH=320
HEIGHT=240

SCALED_WIDTH=160
SCALED_HEIGHT=120

H_RATIO=$((WIDTH / SCALED_WIDTH))
V_RATIO=$((HEIGHT / SCALED_HEIGHT))

ges-launch-1.0 \
  +test-clip smpte s=0 d=2 \
  +test-clip circular s=0 d=2 set-alpha 0.9 \
    set-posx $POSX \
    set-posy $POSY \
  +effect videocrop set-top $CROP_TOP set-bottom $CROP_BOTTOM set-left $CROP_LEFT set-right $CROP_RIGHT \
    set-width $((SCALED_WIDTH - CROP_LEFT / H_RATIO - CROP_RIGHT / H_RATIO)) \
    set-height $((SCALED_HEIGHT - CROP_TOP / V_RATIO - CROP_BOTTOM / V_RATIO))
