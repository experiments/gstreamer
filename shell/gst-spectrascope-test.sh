#!/bin/sh

set -e

# Monitor the default sink
DEFAULT_OUTPUT_MONITOR="$(pacmd stat | grep "Default sink" | cut -d ':' -f 2 | tail -c+2).monitor"

# Take the most recent source as the microphone (in my case it's a USB
# microphone I plugged in after pulseaudio started).
MICROPHONE=$(pactl list short sources | tail -1 | cut -f 2)

set -x
gst-launch-1.0 -v -e \
	compositor name=mix sink_1::xpos=256 sink_2::ypos=256 \
	! 'video/x-raw, width=512, height=512, framerate=(fraction)30/1' \
	! videoconvert \
	! autovideosink \
	pulsesrc device="$DEFAULT_OUTPUT_MONITOR" \
	! deinterleave name=deint \
          deint. \
          ! audioconvert \
          ! spectrascope \
          ! 'video/x-raw, width=256, height=256, framerate=(fraction)30/1' \
          ! textoverlay valignment=center font-desc='Sans, 50' text='Left speaker' \
          ! queue \
          ! mix. \
          deint. \
          ! audioconvert \
          ! spectrascope \
          ! 'video/x-raw, width=256, height=256, framerate=(fraction)30/1' \
          ! textoverlay valignment=center font-desc='Sans, 50' text='Right speaker' \
          ! queue \
          ! mix. \
	pulsesrc device="$MICROPHONE" \
          ! audioconvert \
          ! spectrascope \
          ! 'video/x-raw, width=512, height=256, framerate=(fraction)30/1' \
          ! textoverlay valignment=center font-desc='Sans, 50' text='Microphone' \
          ! queue \
          ! mix.
