#!/bin/bash
#
# Test to check that jpegdec handles different chroma sub-samplings right.

set -e

command -v djpeg &> /dev/null || { echo "djpeg needed, should be in libjpeg-turbo-progs." 1>&2; exit 1; }
command -v cjpeg &> /dev/null || { echo "cjpeg needed, should be in libjpeg-turbo-progs." 1>&2; exit 1; }

if [ "x$1" = "x" ];
then
  WIDTH=996
  HEIGHT=784
  gst-launch-1.0 videotestsrc num-buffers=1 ! "video/x-raw,width=${WIDTH},height=${HEIGHT}" ! jpegenc ! filesink location=videotest.jpg
  INPUT_FILE="videotest.jpg"
else
  INPUT_FILE="$1"
fi

declare -A SAMPLINGS
# 4:1:0 is ambiguous, see https://blog.awm.jp/2016/02/10/yuv/
SAMPLINGS["NORMAL_410"]="4x2,1x1,1x1"
SAMPLINGS["NORMAL_411"]="4x1,1x1,1x1"
SAMPLINGS["NORMAL_420"]="2x2,1x1,1x1"
SAMPLINGS["NORMAL_422"]="2x1,1x1,1x1"
SAMPLINGS["NORMAL_440"]="1x2,1x1,1x1"
SAMPLINGS["NORMAL_444"]="1x1,1x1,1x1"
SAMPLINGS["WEIRD_422"]="2x2,1x2,1x2"
SAMPLINGS["WEIRD_440"]="2x2,2x1,2x1"

for sampling in "${!SAMPLINGS[@]}";
do
  BASE_NAME="${INPUT_FILE##/*}"
  RESAMPLED_BASE_NAME="${BASE_NAME%.*}_${sampling}"

  RESAMPLED_FILE="${RESAMPLED_BASE_NAME}.jpg"
  djpeg "$INPUT_FILE" | cjpeg -sample "${SAMPLINGS[$sampling]}" > "$RESAMPLED_FILE"

  DECODED_RESAMPLED_FILE="decoded_${RESAMPLED_BASE_NAME}_jpegdec.png"
  gst-launch-1.0 filesrc location="$RESAMPLED_FILE" ! jpegparse ! jpegdec     ! videoconvert ! pngenc ! filesink location="$DECODED_RESAMPLED_FILE" || :

  DECODED_RESAMPLED_FILE="decoded_${RESAMPLED_BASE_NAME}_avdec_mjpeg.png"
  gst-launch-1.0 filesrc location="$RESAMPLED_FILE" ! jpegparse ! avdec_mjpeg ! videoconvert ! pngenc ! filesink location="$DECODED_RESAMPLED_FILE" || :

  DECODED_RESAMPLED_FILE="decoded_${RESAMPLED_BASE_NAME}_djpeg.bmp"
  djpeg -bmp "$RESAMPLED_FILE" > "$DECODED_RESAMPLED_FILE"
done

