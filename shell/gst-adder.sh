#!/bin/sh

gst-launch-1.0 adder name=mix ! audioconvert ! autoaudiosink \
  audiotestsrc wave=2 ! mix. \
  audiotestsrc wave=5 ! mix.
