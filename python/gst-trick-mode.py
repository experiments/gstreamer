#!/usr/bin/env python3
#
# A simple "trick-mode" player to play a file at a given speed rate.
#
# Get a test sample with:
# youtube-dl -t http://www.youtube.com/watch?v=yWa-YXiSk2Y

import sys

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
Gst.init(None)

from gi.repository import GLib


class Player:
    def __init__(self, uri, rate):
        self._rate = rate

        self._player = Gst.ElementFactory.make("playbin", "player")
        self._player.set_property("uri", uri)

        bus = self._player.get_bus()
        bus.add_signal_watch()
        bus.connect('message::eos', self.on_eos)
        bus.connect('message::error', self.on_error)
        bus.connect('message::state-changed', self.on_state_changed)

    def run(self):
        self._player.set_state(Gst.State.PLAYING)
        self.loop = GLib.MainLoop()
        self.loop.run()

    def quit(self):
        self._player.set_state(Gst.State.NULL)
        self.loop.quit()

    def on_eos(self, bus, msg):
        self.quit()

    def on_error(self, bus, msg):
        (err, debug) = msg.parse_error()
        print("Error: %s" % err, debug)
        self.quit()

    def on_state_changed(self, bus, msg):
        if msg.src != self._player:
            return

        print('on_state_changed')
        old_state, new_state, pending = msg.parse_state_changed()
        print("%s -> %s" % (old_state, new_state))
        if old_state == Gst.State.READY and new_state == Gst.State.PAUSED:
            self.set_rate(self._rate)

    def set_rate(self, rate):
        self._rate = rate
        position = self._player.query_position(Gst.Format.TIME)[1]

        # Create the seek event
        seek_event = Gst.Event.new_seek(rate,
                                        Gst.Format.TIME,
                                        Gst.SeekFlags.FLUSH | Gst.SeekFlags.ACCURATE,
                                        Gst.SeekType.SET, position,
                                        Gst.SeekType.SET, -1)

        if seek_event:
            self._player.send_event(seek_event)
            Gst.info("rate set to %s" % rate)
        else:
            Gst.warining("change rate failed")


def main(args):
    def usage():
        sys.stdout.write("usage: %s <filename> <speedrate>\n" % args[0])

    if len(args) != 3:
        usage()
        sys.exit(1)

    uri = Gst.filename_to_uri(args[1])
    rate = float(args[2])

    player = Player(uri, rate)
    player.run()

if __name__ == '__main__':
    sys.exit(main(sys.argv))
