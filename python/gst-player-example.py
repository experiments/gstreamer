#!/usr/bin/env python3
#
# Player - a very simple media player based on GstPlayer
#
# Copyright (C) 2016  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import gi

gi.require_version('Gst', '1.0')
from gi.repository import Gst
Gst.init(None)

gi.require_version('GstPlayer', '1.0')
from gi.repository import GstPlayer

from gi.repository import GLib


class Player(object):
    def __init__(self):
        self.player = GstPlayer.Player.new(signal_dispatcher=GstPlayer.PlayerGMainContextSignalDispatcher())
        self.player.connect("error", self.error_cb)
        self.player.connect("end-of-stream", self.end_of_stream_cb)
        self.player.connect("state-changed", self.state_changed_cb)

        self.mainloop = GLib.MainLoop()

    def end_of_stream_cb(self, player):
        self.mainloop.quit()

    def error_cb(self, player, error):
        print(error)
        self.mainloop.quit()

    def state_changed_cb(self, player, state):
        print(state)

    def play(self, filename):
        self.player.set_uri(Gst.filename_to_uri(filename))
        self.player.play()
        self.mainloop.run()

    def stop(self):
        self.player.stop()
        self.mainloop.quit()


def main():
    if len(sys.argv) > 1:
        player = Player()
        try:
            player.play(sys.argv[1])
        except KeyboardInterrupt:
            player.stop()


if __name__ == "__main__":
    main()
