#!/usr/bin/env python3
#
# ges-multi-cut - generate commands to split a video file into multiple clips
#
# Copyright (C) 2016  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys

CLIP_FILENAME_TEMPLATE = "sample_%s.webm"
CLIP_FORMAT = "video/webm:video/x-vp8:audio/x-vorbis"


def timeformat_to_seconds(timeformat):
    """Convert a time string in the format HH:MM:SS.dd to seconds"""
    multipliers = [3600, 60, 1]
    return sum([mult*val for mult, val in
                zip([float(t) for t in timeformat.split(':')], multipliers)])


def parse_samples_list(samples_list_filename):
    """Parse a file in a custom format.
    Each line in the file has this format:

        SAMPLE_NAME HH:MM:SS.ddd HH:MM:SS.ddd

    where sample name is a string and the two time fields are the start time of the
    sample and the end time of the sample.

    Returns a list where each item is a tuple:
        (sample_name, start_time, end_time)
    """
    samples = []

    with open(samples_list_filename, "r") as samples_list_file:
        for row in samples_list_file:
            sample_name, start_timeformat, end_timeformat = row.split()

            start_time = timeformat_to_seconds(start_timeformat)
            end_time = timeformat_to_seconds(end_timeformat)

            samples.append((sample_name, start_time, end_time))

    return samples


def usage():
    print("usage: %s <video_file> <samples_list_file>"
          % os.path.basename(sys.argv[0]))


def main():
    if len(sys.argv) < 3:
        usage()
        return 1

    master_file = sys.argv[1]
    samples_list_filename = sys.argv[2]

    samples = parse_samples_list(samples_list_filename)
    for sample_name, start_time, end_time in samples:
        duration = round(end_time - start_time, 2)
        clip_filename = CLIP_FILENAME_TEMPLATE % sample_name
        print("ges-launch-1.0 +clip \"%s\" inpoint=%s duration=%s -o \"%s\" --format=\"%s\"" %
              (master_file, start_time, duration, clip_filename, CLIP_FORMAT))


if __name__ == "__main__":
    sys.exit(main())
