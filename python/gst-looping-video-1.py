#!/usr/bin/env python3
#
# A simple looping player.
# Version 1, based on EOS handling.
#
# Get a test sample with:
# youtube-dl -t http://www.youtube.com/watch?v=yWa-YXiSk2Y

import sys

import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst
Gst.init(None)

from gi.repository import GLib


class Player:
    def __init__(self, uri):
        self._player = Gst.ElementFactory.make("playbin", "player")
        self._player.set_property("uri", uri)

        bus = self._player.get_bus()
        bus.add_signal_watch()
        bus.connect('message::eos', self.on_eos)
        bus.connect('message::error', self.on_error)

    def run(self):
        self._player.set_state(Gst.State.PLAYING)
        self.loop = GLib.MainLoop()
        self.loop.run()

    def quit(self):
        self._player.set_state(Gst.State.NULL)
        self.loop.quit()

    def on_eos(self, bus, msg):
        sys.stderr.write(".")
        self._player.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH, 0)

    def on_error(self, bus, msg):
        (err, debug) = msg.parse_error()
        print("Error: %s" % err, debug)
        self.quit()


def main(args):
    def usage():
        sys.stdout.write("usage: %s <filename>\n" % args[0])

    if len(args) != 2:
        usage()
        sys.exit(1)

    uri = Gst.filename_to_uri(args[1])

    player = Player(uri)
    player.run()

if __name__ == '__main__':
    sys.exit(main(sys.argv))
